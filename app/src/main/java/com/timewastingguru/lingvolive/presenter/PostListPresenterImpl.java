package com.timewastingguru.lingvolive.presenter;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;

import com.timewastingguru.lingvolive.model.db.DataSource;
import com.timewastingguru.lingvolive.model.db.LingvoLiveContentProvider;
import com.timewastingguru.lingvolive.model.permissions.PermissionsHandler;
import com.timewastingguru.lingvolive.model.rest.RestClient;
import com.timewastingguru.lingvolive.model.rest.requests.GetPostRequests;
import com.timewastingguru.lingvolive.view.PostListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by stepanych on 09.02.16.
 */
public class PostListPresenterImpl implements PostListPresenter, RestClient.RequestListener,
        LoaderManager.LoaderCallbacks<Cursor> {

    public static final int PERMISSIONS_REQUEST_INTERNET = 0;
    private DataSource mDataSource;
    private final RestClient mRestClient;
    private PostListView mPostListView;
    private Timer mTimer = new Timer();

    public PostListPresenterImpl() {
        mRestClient = new RestClient(this);
    }

    @Override
    public void onRequestResult(String result) {
        Log.e("onRequestResult", "" + result);

        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray posts = jsonObject.getJSONArray("posts");

            if (posts.length() > 0) {
                mDataSource.savePosts(posts);
            }

            if (mPostListView != null) {
                mPostListView.getSwipeRefreshLayout().setRefreshing(false);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestFail(Exception e) {
        Log.e("onRequestFail","" + e);

    }

    @Override
    public void onUpdate() {
        executeFeedRequest();
    }

    private void executeFeedRequest() {

        if (android.os.Build.VERSION.SDK_INT > 22) {
            PermissionsHandler.checkInternetPermission(mPostListView.getActivity(), PERMISSIONS_REQUEST_INTERNET,
                    new PermissionsHandler.PermissionListener() {
                        @Override
                        public void doAfterPermissionGranted() {
                            mRestClient.execute(new GetPostRequests());
                        }
                    });
        } else {
            mRestClient.execute(new GetPostRequests());
        }

    }

    @Override
    public void setPostListView(PostListView postListView) {
        mPostListView = postListView;

        if (mDataSource == null) {
            mDataSource = new DataSource(mPostListView.getContext().getApplicationContext());
        }
    }

    @Override
    public void onCreate() {
        executeFeedRequest();
        startUpdatePeriodically();
        mPostListView.getSupportLoaderManager().initLoader(0, null, this);
    }

    private void startUpdatePeriodically() {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        long minuteInMillis = 60 * 1000;

        mTimer.schedule(new FeedUpdateTimerTask(), minuteInMillis, minuteInMillis);
    }

    private void cancelUpdatePeriodically(){
        mTimer.cancel();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {
        mPostListView = null;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mPostListView != null) {
            return new CursorLoader(mPostListView.getContext(), LingvoLiveContentProvider.POSTS, null, null, null, null);
        } else {
            return null;
        }

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.e("PostListPresenter","onLoadFinished " + data.getCount());

        if (mPostListView != null) {
            mPostListView.getAdapter().changeCursor(data);
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (mPostListView != null) {
            mPostListView.getAdapter().changeCursor(null);
        }

    }

    private class FeedUpdateTimerTask extends TimerTask {

        @Override
        public void run() {
            executeFeedRequest();
        }
    }
}
