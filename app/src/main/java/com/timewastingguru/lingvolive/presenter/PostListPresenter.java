package com.timewastingguru.lingvolive.presenter;

import com.timewastingguru.lingvolive.view.PostListView;

/**
 * Created by stepanych on 09.02.16.
 */
public interface PostListPresenter extends BasePresenter {

    void onUpdate();

    void setPostListView(PostListView postListView);

}
