package com.timewastingguru.lingvolive.presenter;

/**
 * Created by stepanych on 09.02.16.
 */
public interface BasePresenter {

    void onCreate();

    void onStart();

    void onResume();

    void onPause();

    void onStop();

    void onDestroy();

}
