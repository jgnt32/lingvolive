package com.timewastingguru.lingvolive.view.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.timewastingguru.lingvolive.R;
import com.timewastingguru.lingvolive.model.entity.Post;
import com.timewastingguru.lingvolive.model.entity.User;

/**
 * Created by stepanych on 10.02.16.
 */
public class PostAdapter extends CursorRecyclerViewAdapter<PostAdapter.PostHolder> {

    public static final int FREE_TYPE = 0;
    public static final int TRANSLATION_REQUEST_TYPE = 1;
    public static final int USER_TRANSLATION_TYPE = 2;

    public PostAdapter(Context context, Cursor cursor) {
        super(context, cursor);
    }

    @Override
    public int getItemViewType(int position) {
        getCursor().moveToPosition(position);
        String postType = getCursor().getString(getCursor().getColumnIndex(Post.PostEntry.POST_TYPE));

        switch (postType) {

            case Post.POST_TYPE_FREE:
                return FREE_TYPE;

            case Post.POST_TYPE_TRANSLATION_REQUEST:
                return TRANSLATION_REQUEST_TYPE;

            case Post.POST_TYPE_USER_TRANSLATION:
                return USER_TRANSLATION_TYPE;

            default: throw new IllegalStateException("Wrong cell type");
        }
    }

    @Override
    public void onBindViewHolder(PostAdapter.PostHolder viewHolder, Cursor cursor) {
        viewHolder.populateView(cursor);
    }

    @Override
    public PostAdapter.PostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case FREE_TYPE:
                return new FreePostHolder(inflate(R.layout.free_post_list_item, parent));

            case TRANSLATION_REQUEST_TYPE:
                return new TranslationHolder(inflate(R.layout.translation_request_post_list_item, parent));

            case USER_TRANSLATION_TYPE:
                return new UserTranslationHolder(inflate(R.layout.user_translation_post_list_item, parent));

            default: throw new IllegalStateException("Wrong cell type");
        }
    }


    public static class FreePostHolder extends PostHolder {

        TextView mMessage;

        public FreePostHolder(View itemView) {
            super(itemView);
            mMessage = (TextView) itemView.findViewById(R.id.post_message);
        }

        @Override
        public void populateView(Cursor cursor) {
            super.populateView(cursor);
            mMessage.setText(cursor.getString(cursor.getColumnIndex(Post.PostEntry.MESSAGE)));
        }
    }


    public static class UserTranslationHolder extends TranslationHolder {

        private TextView mTranslation;

        public UserTranslationHolder(View itemView) {
            super(itemView);
            mTranslation = (TextView) itemView.findViewById(R.id.post_translation);
        }

        @Override
        public void populateView(Cursor cursor){
            super.populateView(cursor);
            String message = cursor.getString(cursor.getColumnIndex(Post.PostEntry.TRANSLATION));
            mTranslation.setText(message);
        }
    }


    public static class TranslationHolder extends PostHolder {

        private TextView mHeading;

        public TranslationHolder(View itemView) {
            super(itemView);
            mHeading = (TextView) itemView.findViewById(R.id.post_heading);
        }

        @Override
        public void populateView(Cursor cursor){
            super.populateView(cursor);
            String heading = cursor.getString(cursor.getColumnIndex(Post.PostEntry.HEADING));
            mHeading.setText(heading);
        }
    }

    public static abstract class PostHolder extends RecyclerView.ViewHolder {

        TextView mUserName;

        public PostHolder(View itemView) {
            super(itemView);
            mUserName = (TextView) itemView.findViewById(R.id.user_name);
        }

        public void populateView(Cursor cursor){
            String userName = cursor.getString(cursor.getColumnIndex(User.UserEntry.NAME));
            mUserName.setText(userName);
        }

    }

}
