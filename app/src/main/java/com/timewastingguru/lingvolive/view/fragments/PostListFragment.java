package com.timewastingguru.lingvolive.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.timewastingguru.lingvolive.R;
import com.timewastingguru.lingvolive.presenter.PostListPresenter;
import com.timewastingguru.lingvolive.presenter.PostListPresenterImpl;
import com.timewastingguru.lingvolive.view.DivederDecoration;
import com.timewastingguru.lingvolive.view.PostListView;
import com.timewastingguru.lingvolive.view.adapters.PostAdapter;

/**
 * Created by stepanych on 10.02.16.
 */
public class PostListFragment extends Fragment implements PostListView {

    private static PostListPresenter mPostListPresenter;

    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private PostAdapter mAdapter ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mAdapter = new PostAdapter(getContext(), null);

        if (mPostListPresenter == null) {
            mPostListPresenter = new PostListPresenterImpl();
        }

        mPostListPresenter.setPostListView(this);
        mPostListPresenter.onCreate();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.posts_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new DivederDecoration(getContext()));

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.pull_to_refresh);
        mSwipeRefreshLayout.setEnabled(true);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPostListPresenter.onUpdate();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPostListPresenter.onDestroy();
    }

    @Override
    public PostAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public LoaderManager getSupportLoaderManager() {
        return getLoaderManager();
    }

    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return mSwipeRefreshLayout;
    }
}
