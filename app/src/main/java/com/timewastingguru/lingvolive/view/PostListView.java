package com.timewastingguru.lingvolive.view;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.LoaderManager;
import android.support.v4.widget.SwipeRefreshLayout;

import com.timewastingguru.lingvolive.view.adapters.PostAdapter;

/**
 * Created by stepanych on 10.02.16.
 */
public interface PostListView {

    PostAdapter getAdapter();

    LoaderManager getSupportLoaderManager();

    SwipeRefreshLayout getSwipeRefreshLayout();

    Context getContext();

    Activity getActivity();
}