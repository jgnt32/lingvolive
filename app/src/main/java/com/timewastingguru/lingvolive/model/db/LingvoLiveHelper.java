package com.timewastingguru.lingvolive.model.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.timewastingguru.lingvolive.model.entity.User.UserEntry;
import com.timewastingguru.lingvolive.model.entity.Post.PostEntry;

/**
 * Created by stepanych on 09.02.16.
 */
public class LingvoLiveHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "LingvoLive.db";

    private static LingvoLiveHelper mInstance;

    private LingvoLiveHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public synchronized static LingvoLiveHelper getInstance(Context context){
        if (mInstance == null) {
            mInstance = new LingvoLiveHelper(context);
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + PostEntry.TABLE_NAME + " ("
                + PostEntry._ID + " INTEGER PRIMARY KEY,"
                + PostEntry.POST_DB_ID + " INTEGER UNIQUE," //unique
                + PostEntry.HEADING + " TEXT, "
                + PostEntry.POST_TYPE + " TEXT, "
                + PostEntry.CREATED_TIME + " LONG,"
                + PostEntry.MESSAGE + " TEXT,"
                + PostEntry.TRANSLATION + " TEXT,"
                + PostEntry.USER_ID + " TEXT, "
                + " FOREIGN KEY (" + PostEntry.USER_ID + ") REFERENCES "
                + UserEntry.TABLE_NAME + "(" + UserEntry.USER_ID + "));");

        db.execSQL("CREATE TABLE " + UserEntry.TABLE_NAME + " ("
                + UserEntry._ID + " INTEGER PRIMARY KEY, "
                + UserEntry.USER_ID + " TEXT UNIQUE, "// unique
                + UserEntry.NAME + " TEXT );");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Migrate if need
    }
}
