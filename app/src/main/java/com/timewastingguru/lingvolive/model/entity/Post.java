package com.timewastingguru.lingvolive.model.entity;

import android.provider.BaseColumns;

/**
 * Created by stepanych on 09.02.16.
 */
public class Post {

    public static final String POST_TYPE_FREE = "Free";
    public static final String POST_TYPE_TRANSLATION_REQUEST = "TranslationRequest";
    public static final String POST_TYPE_USER_TRANSLATION = "UserTranslation";

    public static abstract class PostEntry implements BaseColumns {

        public static final String TABLE_NAME = "post";
        public static final String POST_TYPE = "postType";
        public static final String POST_DB_ID = "postDbId";
        public static final String USER_ID = "user_id";
        public static final String CREATED_TIME = "createdTime";
        public static final String HEADING = "heading";
        public static final String MESSAGE = "message";
        public static final String TRANSLATION = "translation";

    }
}