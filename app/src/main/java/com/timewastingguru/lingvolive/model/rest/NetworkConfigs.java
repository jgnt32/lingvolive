package com.timewastingguru.lingvolive.model.rest;

/**
 * Created by stepanych on 11.02.16.
 */
public class NetworkConfigs {

    public static final String SERVER_ENDPOINT = "http://lingvolive.ru/";
    public static final String PATH_FEED = "/api/social/feed/page";

    public static final String PARAM_PAGE_SIZE = "pageSize";

    public static final String DEFAULT_PARAM_PAGE_SIZE = "10";

}
