package com.timewastingguru.lingvolive.model.rest.requests;

import com.timewastingguru.lingvolive.model.rest.NetworkConfigs;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by stepanych on 09.02.16.
 */
public class GetPostRequests extends BaseGetRequest {

    public GetPostRequests() {
        super(NetworkConfigs.SERVER_ENDPOINT);
    }

    @Override
    protected String getPath() {
        return NetworkConfigs.PATH_FEED;
    }

    @Override
    protected Map<String, String> getParams() {
        Map<String, String> params = new HashMap<>();
        params.put(NetworkConfigs.PARAM_PAGE_SIZE, NetworkConfigs.DEFAULT_PARAM_PAGE_SIZE);
        return params;
    }
}
