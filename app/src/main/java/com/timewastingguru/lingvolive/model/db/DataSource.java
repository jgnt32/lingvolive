package com.timewastingguru.lingvolive.model.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.timewastingguru.lingvolive.model.entity.Post;
import com.timewastingguru.lingvolive.model.entity.Post.PostEntry;
import com.timewastingguru.lingvolive.model.entity.User.UserEntry;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by stepanych on 09.02.16.
 */
public class DataSource {

    private final Context mContext;

    public DataSource(Context context) {
        mContext = context;
    }

    public void savePosts(JSONArray jsonArray){
        new SaveAsyncTask().execute(new SavePostsTask(jsonArray));
    }

    private Date parseDate(String dateString){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        Date convertedDate = null;
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

    private SQLiteDatabase getWritableDatabase() {
        return LingvoLiveHelper.getInstance(mContext).getWritableDatabase();
    }

    private SQLiteDatabase getReadbleDatabase() {
        return LingvoLiveHelper.getInstance(mContext).getReadableDatabase();
    }

    public Cursor getPosts(){
        return getReadbleDatabase().query(PostEntry.TABLE_NAME, null, null, null, null, null, null);
    }

    private class SavePostsTask extends AbstractSaveTask {

        public SavePostsTask(JSONArray jsonArray) {
            this.jsonArray = jsonArray;
        }

        private JSONArray jsonArray;

        @Override
        public int save() {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            int rowCount = 0;

            try {
                writableDatabase.beginTransaction();
                ContentValues contentValues = new ContentValues();

                for (int i = 0; i < jsonArray.length(); i++) {
                    // saving posts
                    JSONObject post = jsonArray.getJSONObject(i);
                    JSONObject user = post.getJSONObject("author");
                    String userId = user.getString(UserEntry.USER_ID);

                    long postDbId = post.getLong(PostEntry.POST_DB_ID);
                    contentValues.put(PostEntry.POST_DB_ID, postDbId);
                    String postType = post.getString(PostEntry.POST_TYPE);
                    contentValues.put(PostEntry.POST_TYPE, postType);

                    if (postType.equalsIgnoreCase(Post.POST_TYPE_FREE)) { // cause in 'Free' posts heading not exist
                        contentValues.put(PostEntry.MESSAGE, post.getString(PostEntry.MESSAGE));
                    } else if (postType.equalsIgnoreCase(Post.POST_TYPE_USER_TRANSLATION)){
                        contentValues.put(PostEntry.TRANSLATION, post.getString(PostEntry.TRANSLATION));
                        contentValues.put(PostEntry.HEADING, post.getString(PostEntry.HEADING));
                    } else {
                        contentValues.put(PostEntry.HEADING, post.getString(PostEntry.HEADING));
                    }

                    contentValues.put(PostEntry.USER_ID, userId);
                    String cratedString = post.getString(PostEntry.CREATED_TIME);
                    Date date = parseDate(cratedString);
                    contentValues.put(PostEntry.CREATED_TIME, date.getTime());
                    int savedPostCount = saveOrUpdate(PostEntry.TABLE_NAME, contentValues, PostEntry.POST_DB_ID + "=?", new String[]{Long.toString(postDbId)});
                    rowCount += savedPostCount;
                    contentValues.clear();

                    // saving user
                    contentValues.put(UserEntry.USER_ID, userId);
                    contentValues.put(UserEntry.NAME, user.getString(UserEntry.NAME));
                    int savedUsersCount = saveOrUpdate(UserEntry.TABLE_NAME, contentValues, UserEntry.USER_ID + "=?", new String[]{userId});
                    rowCount += savedUsersCount;
                    contentValues.clear();
                }
                if (rowCount > 0) {
                    //Notify if was updated post or user
                    mContext.getContentResolver().notifyChange(LingvoLiveContentProvider.POSTS, null, false);
                }
                writableDatabase.setTransactionSuccessful();
            } catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                writableDatabase.endTransaction();
            }
            return rowCount;
        }


    }

    private class SaveAsyncTask extends AsyncTask<AbstractSaveTask, Integer, Integer>{

        @Override
        protected Integer doInBackground(AbstractSaveTask... params) {
            return params[0].save();
        }

    }

    public abstract class AbstractSaveTask {

        abstract int save();

        protected int saveOrUpdate(String tableName, ContentValues contentValues, String whereClause, String[] whereArgs){
            int updateCount = getWritableDatabase().update(tableName, contentValues,
                    whereClause, whereArgs);

            if (updateCount <= 0) {
                getWritableDatabase().insert(tableName, null, contentValues);
                return 1;
            } else {
                return updateCount;
            }
        }

    }


}
