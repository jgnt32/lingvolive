package com.timewastingguru.lingvolive.model.permissions;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by stepanych on 11.02.16.
 */
public class PermissionsHandler {

    public static void checkInternetPermission(Activity activity, int requestCode, PermissionListener listener){
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    requestCode);

        } else {
            listener.doAfterPermissionGranted();
        }
    }

    public interface PermissionListener {
        void doAfterPermissionGranted();
    }

}
