package com.timewastingguru.lingvolive.model.rest.requests;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Artyom Tkachenko on 24.08.2015.
 */
public abstract class BaseGetRequest extends BaseRequest {

    /**
     * @param endpoint server address
     */
    public BaseGetRequest(String endpoint) {
        super(endpoint);
    }



    /** Override of need add GET parameters to request */
    protected Map<String, String> getParams(){
        return null;
    }


    @Override
    public String execute() throws IOException {
        URL url;
        BufferedReader reader = null;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            url = buildUrl();
            URLConnection con = url.openConnection();
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return stringBuilder.toString();
    }

    /**
     * @return string url with all params
     */
    private URL buildUrl() throws MalformedURLException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getEndpoint());
        stringBuilder.append(getPath());
        Map<String, String> params = getParams();

        if (params != null && !params.isEmpty()) {
            Iterator<Map.Entry<String,String>> entries = params.entrySet().iterator();
            stringBuilder.append("?");
            while (entries.hasNext()) {
                Map.Entry<String, String> next = entries.next();
                stringBuilder.append(next.getKey()).append("=").append(next.getValue());
                if (entries.hasNext()){
                    stringBuilder.append("&");
                }
            }
        }
        return new URL(stringBuilder.toString());
    }


}
