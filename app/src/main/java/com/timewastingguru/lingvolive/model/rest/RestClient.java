package com.timewastingguru.lingvolive.model.rest;

import android.os.AsyncTask;
import android.util.Pair;

import com.timewastingguru.lingvolive.model.rest.requests.BaseRequest;
import java.util.ArrayList;

/**
 * Created by stepanych on 09.02.16.
 */
public class RestClient {

    private NetworkAsyncTask mAsyncTask;

    private ArrayList<RequestListener> mRequestListeners = new ArrayList<>();


    public RestClient(RequestListener requestListeners) {
        this.mRequestListeners.add(requestListeners);
    }

    public void execute(BaseRequest request){
        mAsyncTask = new NetworkAsyncTask();
        mAsyncTask.execute(request);
    }

    private class NetworkAsyncTask extends AsyncTask<BaseRequest, Integer, Pair<String, Exception>> {

        @Override
        protected Pair<String, Exception> doInBackground(BaseRequest... params) {

            try {
                return new Pair<>(params[0].execute(), null);
            } catch (Exception e) {
                e.printStackTrace();
                return new Pair<>(null, e);
            }

        }

        @Override
        protected void onPostExecute(Pair<String, Exception> stringExceptionPair) {
            super.onPostExecute(stringExceptionPair);
            notifyListeners(stringExceptionPair);
        }
    }

    private synchronized void notifyListeners(Pair<String, Exception> result){

        for (RequestListener requestListener : mRequestListeners) {

            if (result.second == null) {
                requestListener.onRequestResult(result.first);
            } else {
                requestListener.onRequestFail(result.second);
            }

        }

    }

    public synchronized void subcribe(RequestListener requestListener){
        mRequestListeners.add(requestListener);
    }

    public synchronized void unsubscribe(RequestListener requestListener){
        mRequestListeners.remove(requestListener);
    }

    public interface RequestListener {

        void onRequestResult(String result);

        void onRequestFail(Exception e);

    }


}
