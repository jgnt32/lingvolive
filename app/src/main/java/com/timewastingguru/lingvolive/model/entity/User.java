package com.timewastingguru.lingvolive.model.entity;

import android.provider.BaseColumns;

/**
 * Created by stepanych on 09.02.16.
 */
public class User {

    public static abstract class UserEntry implements BaseColumns {
        public static final String TABLE_NAME = "user";
        public static final String NAME = "name";
        public static final String USER_ID = "id";
    }
}
