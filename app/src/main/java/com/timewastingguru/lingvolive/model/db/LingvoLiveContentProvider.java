package com.timewastingguru.lingvolive.model.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.timewastingguru.lingvolive.model.entity.Post;
import com.timewastingguru.lingvolive.model.entity.User;

/**
 * Created by stepanych on 09.02.16.
 */
public class LingvoLiveContentProvider extends ContentProvider{

    static final String AUTHTORITY = "com.timewastingguru.lingvolive.LingvoLiveContentProvider";
    public final static Uri POSTS = Uri.parse("content://" + AUTHTORITY + "/" + Post.PostEntry.TABLE_NAME);
    public final static Uri USERS = Uri.parse("content://" + AUTHTORITY + "/" + User.UserEntry.TABLE_NAME);

    @Override
    public boolean onCreate() {
        LingvoLiveHelper.getInstance(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
//        Cursor cursor = LingvoLiveHelper.getInstance(getContext()).getReadableDatabase()
//                .query(Post.PostEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
//
        String query = "SELECT * FROM " + Post.PostEntry.TABLE_NAME + " AS p " +
                " INNER JOIN " + User.UserEntry.TABLE_NAME + " AS u " +
                " ON p." + Post.PostEntry.USER_ID + "=u."+ User.UserEntry.USER_ID
                + " ORDER BY p." + Post.PostEntry.CREATED_TIME + " DESC";
        Cursor cursor = LingvoLiveHelper.getInstance(getContext()).getReadableDatabase()
                .rawQuery(query, new String[]{});
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {

        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
