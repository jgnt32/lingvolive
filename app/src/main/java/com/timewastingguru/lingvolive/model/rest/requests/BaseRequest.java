package com.timewastingguru.lingvolive.model.rest.requests;

import java.io.IOException;
import java.util.Map;

/**
 * Base abstract class for executing HTTP requests
 * T - response type
 *
 * Created by Artyom Tkachenko on 24.08.2015.
 */
public abstract class BaseRequest {

    /** Server address */
    private String mEndpoint;

    /**
     * @param endpoint server address
     */
    public BaseRequest(String endpoint) {
        this.mEndpoint = endpoint;
    }

    /**
     * @return server address
     */
    protected String getEndpoint(){
        return mEndpoint;
    }

    /** Return concrete rest method*/
    protected abstract String getPath();

    /**
     * @return clean uri without params
     */
    protected final String getUri(){
        return getEndpoint() + getPath();
    }

    /**
     * @return parsed response
     * @throws IOException @see BaseRequest#retrieveData()
     */
    public abstract String execute() throws IOException;

    /**
     * Close @link java.io.Closeable resources
     */
    protected void close(){

    }

    /**
     * @return headers which you wanna to add into your request
     */
    protected Map<String, String> getHeaders(){
        return null;
    }


}
